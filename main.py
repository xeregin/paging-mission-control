#!/usr/bin/env python3.9

from datetime import datetime

import argparse
import json


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='This program parses telemetry data and displays sensor alerts')
    parser.add_argument('input', help='Path to the telemetry data file.', nargs='?',
                        default='input.txt')
    args = parser.parse_args()
    with open(args.input, 'r') as file:
        contents = file.read().split('\n')
    readings = [x.split('|') for x in contents]
    alerts = []
    alert_data_points = {}
    component_list = ['BATT', 'TSTAT']
    for reading in readings:
        timestamp, sat_id, red_high, yellow_high, yellow_low, red_low, raw, component = reading
        if (float(raw) > float(red_high) and component == 'TSTAT') or \
                (float(raw) < float(red_low) and component == 'BATT'):
            if sat_id not in alert_data_points.keys():
                alert_data_points[sat_id] = []
            if float(raw) > float(red_high):
                severity = 'RED HIGH'
            else:
                severity = 'RED LOW'
            alert_data_points[sat_id].append({
                'severity': severity,
                'component': component,
                'timestamp': timestamp
            })
    for satellite in alert_data_points:
        for comp in component_list:
            tstamps = [x['timestamp'] for x in alert_data_points[satellite] if x['component'] == comp]
            if len(tstamps) >= 3:
                for i in [x for x in range(len(tstamps) - 2)]:
                    if (datetime.strptime(tstamps[0+2], '%Y%m%d %H:%M:%S.%f') -
                            datetime.strptime(tstamps[0], '%Y%m%d %H:%M:%S.%f')).seconds <= 300:
                        if comp == 'BATT':
                            severity = 'RED LOW'
                        else:
                            severity = 'RED HIGH'
                        alerts.append({
                            'satelliteId': int(satellite),
                            'severity': severity,
                            'component': comp,
                            'timestamp': datetime.strptime(tstamps[i],
                                                           '%Y%m%d %H:%M:%S.%f').strftime('%Y-%m-%dT%H:%M:%S.%fZ')
                        })
    print(json.dumps(alerts, indent=4))
